#!/u:sr/bin/env python3
import connexion
import datetime
import logging
import json

from connexion import NoContent

policies={}

def printPolicies():
	print(json.dumps(policies, sort_keys=True, indent=4))

def console():
	return ([policies], 200)

def reset():
	policies.clear()
	return ('ok',200)

def get_healthcheck():
	return ('OK!', 200)


def get_policy_instance_status(policy_id):
	if policy_id in policies:
		return (policies[policy_id],200)
	else:
		return ('No resouce found at the URI', 404)

def delete_policy_instance(policy_id):
	if policy_id in policies:
		policies.pop(policy_id)
		return ('policy instance successfully deleted. {} deleted'.format(policy_id), 204)
	else:
		return ('No resouce found at the URI', 404)

def create_or_update_policy_instance(policy_id,body):
	policies[policy_id]=body
	return ('The updated policy. {}'.format(body), 201)

def get_policy_instance(policy_id):
	if policy_id in policies:
		return (policies[policy_id],200)
	else:
		return ('No resouce found at the URI', 404)

def policynotifications():
	return ('ok',200)

def patch_policies(body):
	return('ok', 200)

def get_all_policy_instances():
	return (list(policies.keys()),200)