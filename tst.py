import connexion

app = connexion.App(__name__, specification_dir='.')
app.add_api('a1-p-openapi.yaml')
app.run(port=8080)
