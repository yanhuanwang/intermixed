
FROM python:3

WORKDIR /usr/src/app

RUN pip install connexion[swagger-ui]

COPY a1.py a1.py
COPY tst.py tst.py
COPY a1-p-common-openapi.yaml a1-p-common-openapi.yaml
COPY a1-p-consumer-openapi.yaml a1-p-consumer-openapi.yaml
COPY a1-p-provider-openapi.yaml a1-p-provider-openapi.yaml

CMD [ "python", "./tst.py" ]